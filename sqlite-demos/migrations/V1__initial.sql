-- events
CREATE TABLE event(
    id INTEGER PRIMARY KEY,
    timestamp INTEGER NOT NULL,
    user_id INTEGER UNIQUE REFERENCES user(id) ON DELETE CASCADE,
    post_id INTEGER UNIQUE REFERENCES post(id) ON DELETE CASCADE
);
CREATE INDEX event_idx ON event(timestamp);

-- users
CREATE TABLE user(
    id INTEGER PRIMARY KEY,
    public_key BLOB NOT NULL UNIQUE,
    public_key_hash BLOB NOT NULL UNIQUE
);
CREATE INDEX user_idx ON user(public_key_hash);

-- posts
CREATE TABLE post(
    id INTEGER PRIMARY KEY,
    body TEXT NOT NULL,
    signature BLOB NOT NULL,
    user_id INTEGER NOT NULL REFERENCES user(id) ON DELETE RESTRICT
);
CREATE VIRTUAL TABLE post_idx USING fts5(body, content='post', content_rowid='id');
CREATE TRIGGER post_ai AFTER INSERT ON post BEGIN
    INSERT INTO post_idx(rowid, body) VALUES(new.id, new.body);
END;
CREATE TRIGGER post_ad AFTER DELETE ON post BEGIN
    INSERT INTO post_idx(post_idx, rowid, body) VALUES('delete', old.id, old.body);
END;
