use rusqlite::{params, Connection, Result};
use std::convert::TryInto;
use std::time::{SystemTime, UNIX_EPOCH};

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!();
}

fn main() {
    let mut conn = Connection::open("data.db").unwrap();

    // Enable Foreign Key Support and Write-Ahead Logging
    conn.pragma_update(None, "foreign_keys", &true).unwrap();
    conn.pragma_update(None, "journal_mode", &"WAL").unwrap();

    // Perform migrations
    embedded::migrations::runner().run(&mut conn).unwrap();

    // Set some sample data
    set_data(&conn).unwrap();

    // Get the data and print it
    get_data(&conn).unwrap();
}

#[derive(Debug)]
struct User {
    public_key: Vec<u8>,
    public_key_hash: [u8; 32],
}

fn set_data(conn: &Connection) -> Result<()> {
    let user = User {
        public_key: vec![1, 2, 3],
        public_key_hash: [1; 32],
    };

    conn.execute(
        "INSERT INTO user(public_key, public_key_hash) VALUES(?1, ?2)",
        params![user.public_key, &user.public_key_hash[..]],
    )?;

    let timestamp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis() as i64;

    conn.execute(
        "INSERT INTO event(timestamp, user_id) VALUES(?1, ?2)",
        params![timestamp, 1],
    )?;

    let post_body = "Hello, world! Here's some text you can search.";

    conn.execute(
        "INSERT INTO post(body, signature, user_id) VALUES(?1, ?2, ?3)",
        params![post_body, vec![1u8, 2, 3], 1],
    )?;

    Ok(())
}

fn get_data(conn: &Connection) -> Result<()> {
    let user = conn.query_row(
        "SELECT public_key, public_key_hash FROM user WHERE id=?1",
        params![1],
        |row| {
            let vec_hash: Vec<u8> = row.get(1)?;
            Ok(User {
                public_key: row.get(0)?,
                public_key_hash: vec_hash.try_into().unwrap(),
            })
        },
    )?;

    println!("Got user: {:?}", user);

    let got_timestamp: i64 = conn.query_row(
        "SELECT timestamp FROM event WHERE user_id=?1",
        params![1],
        |row| Ok(row.get(0)?),
    )?;

    println!("Got event with timestamp: {}", got_timestamp);

    Ok(())
}
