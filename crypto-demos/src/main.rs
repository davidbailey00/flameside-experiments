use openssl::pkey::{PKey, Private};
use rand::prelude::*;

fn sign_and_verify(keypair: &PKey<Private>) {
    use openssl::hash::MessageDigest;
    use openssl::sign::{Signer, Verifier};

    let data = "Hola, mundo".as_bytes();
    let data2 = "Hello, 世界".as_bytes();

    // Sign the data
    let mut signer = Signer::new(MessageDigest::sha256(), keypair).unwrap();
    signer.update(data).unwrap();
    signer.update(data2).unwrap();
    let signature = signer.sign_to_vec().unwrap();

    // Verify the data
    let mut verifier = Verifier::new(MessageDigest::sha256(), keypair).unwrap();
    verifier.update(data).unwrap();
    verifier.update(data2).unwrap();
    let is_valid = verifier.verify(&signature).unwrap();

    println!("Valid signature: {}", is_valid);
}

fn encrypt_and_decrypt(keypair1: &PKey<Private>, keypair2: &PKey<Private>) {
    use openssl::derive::Deriver;

    let mut deriver1 = Deriver::new(keypair1).unwrap();
    deriver1.set_peer(keypair2).unwrap();
    let secret1 = deriver1.derive_to_vec().unwrap();

    let mut deriver2 = Deriver::new(keypair2).unwrap();
    deriver2.set_peer(keypair1).unwrap();
    let secret2 = deriver2.derive_to_vec().unwrap();

    assert_eq!(secret1, secret2);
    println!("Shared secret: {:?}", secret1);
}

fn derive_username(keypair: &PKey<Private>) {
    use openssl::sha::Sha256;
    use parity_wordlist::WORDS;

    let public_bytes = keypair.public_key_to_der().unwrap();

    let mut sha256 = Sha256::new();
    sha256.update(&public_bytes);

    let hash = sha256.finish();
    let mut seeded_rng = StdRng::from_seed(hash);

    let username: Vec<_> = (0..6)
        .map(|_| *WORDS.choose(&mut seeded_rng).unwrap())
        .collect();
    println!("Derived username: {}", username.join(" "));
}

fn main() {
    use openssl::ec::{EcGroup, EcKey};
    use openssl::nid::Nid;

    // Generate a secp256k1 group
    let group = EcGroup::from_curve_name(Nid::SECP256K1).unwrap();

    // Generate a keypair
    let keypair1 = EcKey::generate(&group).unwrap();
    let keypair1 = PKey::from_ec_key(keypair1).unwrap();

    // Generate another keypair
    let keypair2 = EcKey::generate(&group).unwrap();
    let keypair2 = PKey::from_ec_key(keypair2).unwrap();

    // Print the public keys
    let public_key1 = keypair1.public_key_to_pem().unwrap();
    let public_key2 = keypair2.public_key_to_pem().unwrap();
    println!(
        "Key 1: {}\nKey 2: {}",
        String::from_utf8(public_key1).unwrap(),
        String::from_utf8(public_key2).unwrap()
    );

    // Run demo functions
    encrypt_and_decrypt(&keypair1, &keypair2);
    sign_and_verify(&keypair1);
    derive_username(&keypair1);
}
