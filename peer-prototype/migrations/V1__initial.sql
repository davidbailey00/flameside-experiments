-- config
CREATE TABLE config(
    key         TEXT    PRIMARY KEY,
    value       BLOB
);

-- events
CREATE TABLE event(
    id          INTEGER PRIMARY KEY,
    hopdist     INTEGER NOT NULL    DEFAULT 0,
    timestamp   INTEGER NOT NULL,
    hash        BLOB    NOT NULL UNIQUE,
    user_id     INTEGER UNIQUE      REFERENCES user(id) ON DELETE CASCADE,
    post_id     INTEGER UNIQUE      REFERENCES post(id) ON DELETE CASCADE
);
CREATE INDEX event_idx ON event(timestamp);

-- users
CREATE TABLE user(
    id          INTEGER PRIMARY KEY,
    pubkey      BLOB    NOT NULL UNIQUE
);

-- posts
CREATE TABLE post(
    id          INTEGER PRIMARY KEY,
    signature   BLOB    NOT NULL,
    author      INTEGER NOT NULL    REFERENCES user(id) ON DELETE RESTRICT,
    replyto     BLOB,               -- event.hash for post events
    body        TEXT    NOT NULL
);
