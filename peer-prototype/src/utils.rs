use std::time::{SystemTime, UNIX_EPOCH};

pub fn timestamp_now() -> i64 {
    // i64 because SQLite can't handle u64 but we need
    // more than 32 bits to avoid the Y2038 problem
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis() as i64
}
