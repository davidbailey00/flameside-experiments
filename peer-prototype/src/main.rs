use crate::events::Post;
use crate::utils::timestamp_now;
use anyhow::Result;
use rusqlite::Connection;
use std::env;

mod events;
mod setup;
mod utils;

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!();
}

fn main() -> Result<()> {
    let mut conn = Connection::open(env::var("DB_PATH")?)?;
    conn.pragma_update(None, "foreign_keys", &true)?;
    conn.pragma_update(None, "journal_mode", &"WAL")?;

    // Perform migrations
    embedded::migrations::runner().run(&mut conn)?;

    // Perform initial setup
    let (keypair, pubkey_hash) = setup::setup(&mut conn)?;

    // Create a post and insert it
    let post = Post {
        timestamp: timestamp_now(),
        author: pubkey_hash,
        replyto: None,
        body: "film is a subcategory of animation in which still pictures appear to move".into(),
    };
    post.insert(&mut conn, &keypair)?;

    Ok(())
}
