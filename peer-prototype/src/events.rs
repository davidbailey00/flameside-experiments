use anyhow::Result;
use openssl::{
    hash::MessageDigest,
    pkey::{PKey, Private},
    sha::sha256,
    sign::Signer,
};
use rusqlite::{params, Connection};
use serde::Serialize;

#[derive(Serialize)]
pub struct Post {
    pub timestamp: i64,
    pub author: [u8; 32],
    pub replyto: Option<[u8; 32]>,
    pub body: String,
}

impl Post {
    pub fn insert(&self, conn: &mut Connection, keypair: &PKey<Private>) -> Result<()> {
        // Generate a hash of the `post` struct
        let encoded = bincode::serialize(self).unwrap();
        let hash = sha256(&encoded);

        // Sign the `post` struct with the user's key
        let mut signer = Signer::new(MessageDigest::sha256(), keypair).unwrap();
        signer.update(&encoded).unwrap();
        let signature = signer.sign_to_vec().unwrap();

        let replyto_vec = match self.replyto {
            Some(replyto) => Some(replyto.to_vec()),
            None => None,
        };

        let tx = conn.transaction()?;

        tx.execute(
            "INSERT INTO post(signature, author, replyto, body) \
            VALUES(?1, (SELECT user_id FROM event WHERE hash = ?2), ?3, ?4)",
            params![signature, self.author.to_vec(), replyto_vec, self.body],
        )?;

        let post_id = tx.last_insert_rowid();
        tx.execute(
            "INSERT INTO event(timestamp, hash, post_id) VALUES(?1, ?2, ?3)",
            params![self.timestamp, hash.to_vec(), post_id],
        )?;

        // update the post author event so it is newer than the
        // post itself, and therefore distributed to peers first
        tx.execute(
            "UPDATE event SET timestamp = ?1 WHERE hash = ?2",
            params![self.timestamp + 1, self.author.to_vec()],
        )?;

        tx.commit()?;
        Ok(())
    }
}
