use crate::utils::timestamp_now;
use anyhow::Result;
use openssl::{
    ec::{EcGroup, EcKey},
    nid::Nid,
    pkey::{PKey, Private},
    sha::sha256,
};
use rusqlite::{params, Connection};
use std::convert::TryInto;

pub fn setup(conn: &mut Connection) -> Result<(PKey<Private>, [u8; 32])> {
    let initialized: bool = conn.query_row(
        "SELECT EXISTS(SELECT 1 FROM config WHERE key = 'initialized')",
        params![],
        |row| row.get(0),
    )?;

    if initialized {
        regular_setup(conn)
    } else {
        initial_setup(conn)
    }
}

fn regular_setup(conn: &Connection) -> Result<(PKey<Private>, [u8; 32])> {
    let privkey: Vec<u8> = conn.query_row(
        "SELECT value FROM config WHERE key = 'privkey'",
        params![],
        |row| row.get(0),
    )?;

    let hash: Vec<u8> = conn.query_row(
        "SELECT value FROM config WHERE key = 'userhash'",
        params![],
        |row| row.get(0),
    )?;

    let keypair = PKey::private_key_from_der(&privkey).unwrap();
    let hash: [u8; 32] = hash.try_into().unwrap();
    Ok((keypair, hash))
}

fn initial_setup(conn: &mut Connection) -> Result<(PKey<Private>, [u8; 32])> {
    // Create a secp256k1 group
    let group = EcGroup::from_curve_name(Nid::SECP256K1).unwrap();

    // Generate a keypair
    let keypair = EcKey::generate(&group).unwrap();
    let keypair = PKey::from_ec_key(keypair).unwrap();

    // Get private and public keys in DER format
    let privkey = keypair.private_key_to_der().unwrap();
    let pubkey = keypair.public_key_to_der().unwrap();

    let hash = sha256(&pubkey);
    let timestamp = timestamp_now();

    let tx = conn.transaction()?;

    // indicate completion of initial setup
    tx.execute("INSERT INTO config(key) VALUES ('initialized')", params![])?;

    tx.execute(
        "INSERT INTO config(key, value) VALUES('privkey', ?1)",
        params![privkey],
    )?;

    tx.execute(
        "INSERT INTO config(key, value) VALUES('userhash', ?1)",
        params![hash.to_vec()],
    )?;

    tx.execute("INSERT INTO user(pubkey) VALUES(?1)", params![pubkey])?;
    let user_id = tx.last_insert_rowid();

    tx.execute(
        "INSERT INTO event(timestamp, hash, user_id) VALUES(?1, ?2, ?3)",
        params![timestamp, hash.to_vec(), user_id],
    )?;

    tx.commit()?;
    Ok((keypair, hash))
}
