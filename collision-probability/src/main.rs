use std::f32::consts;

fn collision_probability(num_days: u32, num_people: u32) -> f32 {
    let inverse_probability: f32 = (1..num_people)
        .map(|i| 1.0 - (i as f32 / num_days as f32))
        .product();
    1.0 - inverse_probability
}

fn collision_probability_approx(num_days: u128, num_people: u128) -> f32 {
    let num_pairs = num_people * (num_people - 1) / 2;
    let exponent = -(num_pairs as f32 / num_days as f32);
    1.0 - consts::E.powf(exponent)
}

fn main() {
    let num_days = 365;
    let num_people = 23;

    println!(
        "Probability of collision, given a {} days and {} people: {}",
        num_days,
        num_people,
        collision_probability(num_days, num_people)
    );

    let num_days = 7776u128.pow(6); // 6 random words
    let num_people = 7_674_000_000; // world population

    println!(
        "Probability of collision, given a {} possibilities and {} items: {}",
        num_days,
        num_people,
        collision_probability_approx(num_days, num_people)
    );
}
